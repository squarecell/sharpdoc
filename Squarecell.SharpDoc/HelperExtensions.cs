﻿using Squarecell.SharpDoc.Contract.Protocol;

namespace Squarecell.SharpDoc;

public static class HelperExtensions
{
    public static PdfGeneratorOptionsBuilder<TModel> AddSimpleKeywordRule<TModel>(
        this PdfGeneratorOptionsBuilder<TModel> @this,
        string keyword,
        Func<TModel, string> func
    ) =>
        @this.AddParserRule(new SimpleKeywordRule<TModel>(keyword, func));

    public static PdfGeneratorOptionsBuilder<TModel> AddKeywordAndIdentifierRule<TModel>(
        this PdfGeneratorOptionsBuilder<TModel> @this,
        string keyword,
        Func<string, TModel, string> func
    ) =>
        @this.AddParserRule(new KeywordWithIdentifierRule<TModel>(keyword, func));
}
