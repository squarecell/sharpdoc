﻿using Squarecell.SharpDoc.Contract.Ports;
using Squarecell.SharpDoc.Contract.Protocol;

namespace Squarecell.SharpDoc;

public class PdfGenerator : IGeneratePdf
{
    private readonly IConvertHtmlToPdf _converter;
    private readonly IRenderHtmlTemplate _templateRenderer;

    public PdfGenerator(IRenderHtmlTemplate templateRenderer, IConvertHtmlToPdf converter)
    {
        _templateRenderer = templateRenderer;
        _converter = converter;
    }

    #region IGeneratePdf Members

    public async Task<byte[]> GenerateDocument<TModel>(PdfGeneratorOptions<TModel> options)
    {
        var (_, ms) = await GenerateDocumentAndIntermediateHtml(options);

        return ms;
    }

    public async Task<FinalHtmlWithDoc> GenerateDocumentAndIntermediateHtml<TModel>(PdfGeneratorOptions<TModel> options)
    {
        var renderResults = await _templateRenderer.RenderTemplate(
            options.TemplateText,
            options.Model,
            options.AdditionalParserRules
        );

        var results = await _converter.ConvertToPdf(renderResults.FinalHtml);
        using var ms = new MemoryStream(results.DocBytes);

        ms.Position = 0;

        return new FinalHtmlWithDoc(renderResults.FinalHtml, ms.ToArray());
    }

    #endregion
}
