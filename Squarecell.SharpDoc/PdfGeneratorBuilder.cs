﻿using Squarecell.SharpDoc.Contract.Ports;

namespace Squarecell.SharpDoc;

public class PdfGeneratorBuilderContext
{
    public IRenderHtmlTemplate? TemplateRenderer { get; set; }
    public IConvertHtmlToPdf? Converter { get; set; }
}

public class PdfGeneratorBuilder
{
    internal readonly PdfGeneratorBuilderContext Context = new();

    public PdfGeneratorBuilder WithTemplateRenderer(IRenderHtmlTemplate templateRenderer)
    {
        Context.TemplateRenderer = templateRenderer;

        return this;
    }

    public PdfGeneratorBuilder WithConverter(IConvertHtmlToPdf converter)
    {
        Context.Converter = converter;

        return this;
    }

    public PdfGenerator Build() =>
        new(
            Context.TemplateRenderer ?? throw PdfGeneratorBuilderException.ForMissingRenderer(),
            Context.Converter ?? throw PdfGeneratorBuilderException.ForMissingConverter()
        );
}

public class PdfGeneratorBuilderException : Exception
{
    private PdfGeneratorBuilderException(string msg) : base(msg) { }

    public static PdfGeneratorBuilderException ForMissingRenderer() =>
        new($"TemplateRenderer is null. Did you forget to call {nameof(PdfGeneratorBuilder.WithTemplateRenderer)}");

    public static PdfGeneratorBuilderException ForMissingConverter() =>
        new($"Converter is null. Did you forget to call {nameof(PdfGeneratorBuilder.WithConverter)}");
}
