﻿using Squarecell.SharpDoc.Contract.Protocol;

namespace Squarecell.SharpDoc;

internal class PdfGeneratorOptionsBuilderContext<TModel>
{
    public TModel? Model { get; set; }
    public string? TemplateText { get; set; }

    public List<IAdditionalParserRule> AdditionalParserRules { get; } = new();
}

public class PdfGeneratorOptionsBuilder<TModel>
{
    internal readonly PdfGeneratorOptionsBuilderContext<TModel> Context = new();

    public PdfGeneratorOptionsBuilder<TModel> WithTemplate(string templateText)
    {
        Context.TemplateText = templateText;

        return this;
    }

    public PdfGeneratorOptionsBuilder<TModel> WithModel(TModel model)
    {
        Context.Model = model;

        return this;
    }

    public PdfGeneratorOptionsBuilder<TModel> AddParserRule(IAdditionalParserRule rule)
    {
        Context.AdditionalParserRules.Add(rule);

        return this;
    }

    public PdfGeneratorOptionsBuilder<TModel> AddParserRules(params IAdditionalParserRule[] rules)
    {
        foreach (var rule in rules)
            AddParserRule(rule);

        return this;
    }

    public PdfGeneratorOptions<TModel> Build() =>
        new(
            Context.TemplateText ?? throw PdfGeneratorOptionsBuilderException.ForMissingTemplate(),
            Context.Model ?? throw PdfGeneratorOptionsBuilderException.ForMissingModel(),
            Context.AdditionalParserRules.ToArray()
        );
}

public class PdfGeneratorOptionsBuilderException : Exception
{
    private PdfGeneratorOptionsBuilderException(string msg) : base(msg) { }

    public static PdfGeneratorOptionsBuilderException ForMissingTemplate() =>
        new(
            $"TemplateRenderer is null. Did you forget to call {nameof(PdfGeneratorOptionsBuilder<object>.WithTemplate)}"
        );

    public static PdfGeneratorOptionsBuilderException ForMissingModel() =>
        new($"TemplateRenderer is null. Did you forget to call {nameof(PdfGeneratorOptionsBuilder<object>.WithModel)}");
}
