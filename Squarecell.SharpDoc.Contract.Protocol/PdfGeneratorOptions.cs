﻿namespace Squarecell.SharpDoc.Contract.Protocol;

public record PdfGeneratorOptions<TModel>(string TemplateText, TModel Model)
{
    public readonly IAdditionalParserRule[] AdditionalParserRules = Array.Empty<IAdditionalParserRule>();

    public PdfGeneratorOptions(
        string templateText,
        TModel model,
        params IAdditionalParserRule[] additionalParserRules
    ) : this(templateText, model)
    {
        AdditionalParserRules = additionalParserRules;
    }
}
