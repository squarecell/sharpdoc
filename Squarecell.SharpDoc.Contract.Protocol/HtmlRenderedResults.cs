﻿namespace Squarecell.SharpDoc.Contract.Protocol;

public record HtmlRenderedResults(string FinalHtml);

public record FinalHtmlWithDoc(string FinalHtml, byte[] Doc);
