﻿namespace Squarecell.SharpDoc.Contract.Protocol;

public record HtmlToPdfResults(byte[] DocBytes);
