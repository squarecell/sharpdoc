﻿namespace Squarecell.SharpDoc.Contract.Protocol;

public interface IAdditionalParserRule
{
    public string Keyword { get; }
}

public record SimpleKeywordRule<TModel>(string Keyword, Func<TModel, string> Func) : IAdditionalParserRule;

public record KeywordWithIdentifierRule<TModel>(
    string Keyword,
    Func<string, TModel, string> Func
) : IAdditionalParserRule;
