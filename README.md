[![pipeline status](https://gitlab.com/squarecell/sharpdoc/badges/main/pipeline.svg)](https://gitlab.com/squarecell/sharpdoc/-/commits/main)
[![coverage report](https://gitlab.com/squarecell/sharpdoc/badges/main/coverage.svg)](https://gitlab.com/squarecell/sharpdoc/-/commits/main)

# SharpDoc

SharpDoc is a .NET 6 wrapper around Fluid.Core and PuppeteerSharp
allowing easy PDF generation from HTML templates.

# Requirements

PuppeteerSharp uses chrome to generate PDFs. This version of SharpDoc
**does not include chrome or its dependencies**; it is assumed that
the system running your modules using SharpDoc are able to run chrome.

# Usage

By default, SharpDoc will look for an environment variable
named `SHARPDOC_CHROME_PATH` to use as a chrome executable.