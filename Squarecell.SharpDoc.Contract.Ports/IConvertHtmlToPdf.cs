﻿using Squarecell.SharpDoc.Contract.Protocol;

namespace Squarecell.SharpDoc.Contract.Ports;

public interface IConvertHtmlToPdf
{
    public Task<HtmlToPdfResults> ConvertToPdf(string finalHtml);
}
