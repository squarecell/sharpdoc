﻿using Squarecell.SharpDoc.Contract.Protocol;

namespace Squarecell.SharpDoc.Contract.Ports;

public interface IGeneratePdf
{
    public Task<byte[]> GenerateDocument<TModel>(PdfGeneratorOptions<TModel> options);

    public Task<FinalHtmlWithDoc> GenerateDocumentAndIntermediateHtml<TModel>(PdfGeneratorOptions<TModel> options);
}
