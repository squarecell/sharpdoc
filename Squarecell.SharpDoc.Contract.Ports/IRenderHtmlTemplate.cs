﻿using Squarecell.SharpDoc.Contract.Protocol;

namespace Squarecell.SharpDoc.Contract.Ports;

public interface IRenderHtmlTemplate
{
    public Task<HtmlRenderedResults> RenderTemplate<TModel>(
        string template,
        TModel model,
        IEnumerable<IAdditionalParserRule> additionalParserRules
    );
}
