﻿using System.Text;
using Squarecell.SharpDoc.Contract.Ports;
using Squarecell.SharpDoc.Contract.Protocol;
using Squarecell.SharpDoc.TestLib;

namespace Squarecell.SharpDoc.Acceptance.Tests;

[TestFixture]
public class BasicPlumbingTests
{
    #region Setup/Teardown

    [OneTimeSetUp]
    public void OneTimeSetup()
    {
        _renderer = Substitute.For<IRenderHtmlTemplate>();
        _converter = Substitute.For<IConvertHtmlToPdf>();

        _renderer
            .RenderTemplate(Arg.Any<string>(), Arg.Any<object>(), Arg.Any<IAdditionalParserRule[]>())
            .Returns(info => new HtmlRenderedResults((string) info[0]));
        _converter
            .ConvertToPdf(Arg.Any<string>())
            .Returns(info => new HtmlToPdfResults(Encoding.ASCII.GetBytes((string) info[0])));
    }

    #endregion

    private IConvertHtmlToPdf _converter = null!;
    private IRenderHtmlTemplate _renderer = null!;

    [Test]
    public void Empty_builder_should_throw()
    {
        var act = () => new PdfGeneratorBuilder().Build();

        act.Should()
            .Throw<PdfGeneratorBuilderException>();

        act = () => new PdfGeneratorBuilder()
            .WithConverter(_converter)
            .Build();

        act.Should()
            .Throw<PdfGeneratorBuilderException>();

        act = () => new PdfGeneratorBuilder()
            .WithTemplateRenderer(_renderer)
            .Build();

        act.Should()
            .Throw<PdfGeneratorBuilderException>();

        var actOptions = () => new PdfGeneratorOptionsBuilder<DummyModel>()
            .Build();

        actOptions
            .Should()
            .Throw<PdfGeneratorOptionsBuilderException>();

        actOptions = () => new PdfGeneratorOptionsBuilder<DummyModel>()
            .WithModel(new DummyModel())
            .Build();

        actOptions
            .Should()
            .Throw<PdfGeneratorOptionsBuilderException>();

        actOptions = () => new PdfGeneratorOptionsBuilder<DummyModel>()
            .WithTemplate("")
            .Build();

        actOptions
            .Should()
            .Throw<PdfGeneratorOptionsBuilderException>();

        actOptions = () => new PdfGeneratorOptionsBuilder<DummyModel>()
            .AddParserRules()
            .Build();

        actOptions
            .Should()
            .Throw<PdfGeneratorOptionsBuilderException>();
    }

    [Test]
    public async Task Providing_dummy_implementation_should_return_same_doc()
    {
        var generator = new PdfGeneratorBuilder()
            .WithConverter(_converter)
            .WithTemplateRenderer(_renderer)
            .Build();

        const string htmlTemplate = """
<html>
<body>
    <p>Hello!</p>
</body>
</html>
""";

        var generatorOptions = new PdfGeneratorOptionsBuilder<DummyModel>()
            .WithModel(new DummyModel())
            .WithTemplate(htmlTemplate)
            .Build();

        var doc = await generator.GenerateDocument(generatorOptions);
        var outStr = new string(Encoding.ASCII.GetString(doc.ToArray()));

        outStr
            .Should()
            .Be(htmlTemplate);
    }
}
