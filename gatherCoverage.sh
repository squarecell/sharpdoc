#!/bin/sh

line_rate=$(head -n 2 "$1" | sed 'N;s/.*line-rate="\([^" ]*\).*/\1/g')

result=$(echo "$line_rate * 100" | bc)

printf "TOTAL_COVERAGE=%2.2f\n" "$result"