﻿FROM zenika/alpine-chrome AS sharpdoc-test-env-setup

USER root

COPY ./gatherCoverage.sh /opt/scripts/gatherCoverage.sh

RUN apk add --no-cache bash icu-libs krb5-libs libgcc libintl libssl1.1 libstdc++ zlib wget
RUN apk add --no-cache libgdiplus --repository https://dl-3.alpinelinux.org/alpine/edge/testing/

RUN mkdir -p /usr/share/dotnet \
    && ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet 

RUN wget https://dot.net/v1/dotnet-install.sh
RUN chmod +x dotnet-install.sh
RUN ./dotnet-install.sh -c 6.0 --install-dir /usr/share/dotnet
RUN rm dotnet-install.sh

SHELL ["/bin/bash", "-c"]

RUN addgroup -S appuser && adduser -S appuser -G appuser -s /bin/bash

RUN mkdir -p /home/appuser/.dotnet
RUN chown appuser:appuser -R /home/appuser
RUN chmod +x /opt/scripts/gatherCoverage.sh
RUN chown appuser:appuser /opt/scripts/gatherCoverage.sh

ENTRYPOINT []

FROM sharpdoc-test-env-setup AS sharpdoc-test-env

WORKDIR /home/appuser
USER appuser

RUN dotnet tool install --global dotnet-coverage

ENV SHARPDOC_CHROME_PATH=/usr/bin/chromium-browser
ENV PATH=$PATH:/home/appuser/.dotnet/tools
