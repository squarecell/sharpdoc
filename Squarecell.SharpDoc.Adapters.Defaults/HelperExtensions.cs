﻿namespace Squarecell.SharpDoc.Adapters.Defaults;

public static class HelperExtensions
{
    public static PdfGeneratorBuilder WithDefaults(this PdfGeneratorBuilder @this) =>
        @this
            .WithTemplateRenderer(new FluidCoreTemplateRenderer())
            .WithConverter(new PuppeteerSharpConverter());
}
