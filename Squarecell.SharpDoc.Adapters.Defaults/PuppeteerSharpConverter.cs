﻿using PuppeteerSharp;
using PuppeteerSharp.Media;
using Squarecell.SharpDoc.Contract.Ports;
using Squarecell.SharpDoc.Contract.Protocol;

namespace Squarecell.SharpDoc.Adapters.Defaults;

public class PuppeteerSharpConverter : IConvertHtmlToPdf
{
    private const string ChromePathEnvVarName = "SHARPDOC_CHROME_PATH";

    #region IConvertHtmlToPdf Members

    public async Task<HtmlToPdfResults> ConvertToPdf(string finalHtml)
    {
        var options = new LaunchOptions
        {
            Headless = true,
            Args = new[]
            {
                "--proxy-server=direct://", "--proxy-bypass-list=*", "--no-sandbox", "--disable-gpu",
                "--disable-software-rasterizer", "--disable-dev-shm-usage"
            }
        };

#if !SHARPDOC_REUSE_CHROME
        if (string.IsNullOrEmpty(Environment.GetEnvironmentVariable(ChromePathEnvVarName)))
        {
            using var browserFetcher = new BrowserFetcher();
            await browserFetcher.DownloadAsync(BrowserFetcher.DefaultChromiumRevision);
        }
#else
        if (string.IsNullOrEmpty(Environment.GetEnvironmentVariable(ChromePathEnvVarName)))
            throw PuppeteerSharpConverterException.ForMissingEnvVar(ChromePathEnvVarName);
        options.ExecutablePath = Environment.GetEnvironmentVariable(ChromePathEnvVarName);
#endif
        await using var browser = await Puppeteer.LaunchAsync(options);
        var page = await browser.NewPageAsync();
        await page.SetContentAsync(finalHtml);

        await using var s = await page.PdfStreamAsync(new PdfOptions {Landscape = false, Format = PaperFormat.A4});
        using var ms = new MemoryStream();

        await s.CopyToAsync(ms);

        return new HtmlToPdfResults(ms.ToArray());
    }

    #endregion
}

public class PuppeteerSharpConverterException : Exception
{
    private PuppeteerSharpConverterException(string msg) : base(msg) { }

    public static PuppeteerSharpConverterException ForMissingEnvVar(string envVarName) =>
        new($"Environment variable {envVarName} not found");
}
