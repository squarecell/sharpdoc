using Fluid;
using Fluid.Ast;
using Squarecell.SharpDoc.Contract.Ports;
using Squarecell.SharpDoc.Contract.Protocol;

namespace Squarecell.SharpDoc.Adapters.Defaults;

public class FluidCoreTemplateRenderer : IRenderHtmlTemplate
{
    private readonly FluidParser _parser = new();

    #region IRenderHtmlTemplate Members

    public async Task<HtmlRenderedResults> RenderTemplate<TModel>(
        string template,
        TModel model,
        IEnumerable<IAdditionalParserRule> additionalParserRules
    )
    {
        foreach (var additionalParserRule in additionalParserRules)
            switch (additionalParserRule)
            {
                case SimpleKeywordRule<TModel> (var keyword, var func):
                    _parser.RegisterEmptyTag(
                        keyword,
                        async (writer, _, _) =>
                        {
                            await writer.WriteAsync(func(model));

                            return Completion.Normal;
                        }
                    );
                    break;

                case KeywordWithIdentifierRule<TModel> (var keyword, var func):
                    _parser.RegisterIdentifierTag(
                        keyword,
                        async (identifier, writer, _, _) =>
                        {
                            await writer.WriteAsync(func(identifier, model));

                            return Completion.Normal;
                        }
                    );
                    break;
            }

        if (!_parser.TryParse(template, out var finalTemplate, out var error))
            throw FluidCoreTemplateRendererException.ForParsingError(error);

        var options = new TemplateOptions {MemberAccessStrategy = new UnsafeMemberAccessStrategy()};
        var context = new TemplateContext(model, options);

        return new HtmlRenderedResults(await finalTemplate.RenderAsync(context));
    }

    #endregion

    #region Nested type: FluidCoreTemplateRendererException

    public sealed class FluidCoreTemplateRendererException : Exception
    {
        private FluidCoreTemplateRendererException(string msg) : base(msg) { }

        public static FluidCoreTemplateRendererException ForParsingError(string error) =>
            new($"Error in html template parsing: {error}");
    }

    #endregion
}
