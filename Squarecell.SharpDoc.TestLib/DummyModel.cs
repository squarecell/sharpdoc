﻿namespace Squarecell.SharpDoc.TestLib;

public sealed record SimplePage(string Title, string Base64Logo, string Base64LogoAlt);

public sealed record DummyModel;
