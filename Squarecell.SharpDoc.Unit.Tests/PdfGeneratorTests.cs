﻿// Uncomment next line to enable generating resulting PDF files
// in the %APPDATA%\PdfGenerator folder on Windows or ~/.config/PdfGenerator on Unix
// #define GenerateTestOutputFiles

using Squarecell.SharpDoc.Adapters.Defaults;
using Squarecell.SharpDoc.Contract.Protocol;
using Squarecell.SharpDoc.TestLib;

namespace Squarecell.SharpDoc.Unit.Tests;

[TestFixture]
public class PdfGeneratorTests
{
#if GenerateTestOutputFiles
    private static string _tmpPath = "";
#endif

    [OneTimeSetUp]
    public void OneTimeSetup()
    {
#if GenerateTestOutputFiles
        _tmpPath = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
            "PdfGenerator"
        );

        if (!Directory.Exists(_tmpPath))
            Directory.CreateDirectory(_tmpPath);
#endif
    }

    [Test]
    public async Task Simple_doc_should_generate_without_errors()
    {
        var generator = new PdfGeneratorBuilder()
            .WithDefaults()
            .Build();

        var htmlTemplate = await File.ReadAllTextAsync("./Templates/HelloHtmlConvert.html");

        var generatorOptions = new PdfGeneratorOptionsBuilder<DummyModel>()
            .WithModel(new DummyModel())
            .WithTemplate(htmlTemplate)
            .Build();

        var doc = await generator.GenerateDocument(generatorOptions);

        doc
            .Should()
            .NotBeNullOrEmpty();

        var ex = PuppeteerSharpConverterException.ForMissingEnvVar("test");

        ex
            .Should()
            .NotBeNull();

#if GenerateTestOutputFiles
        var finalPath = Path.Combine(_tmpPath, "simple_doc_test.pdf");
        await File.WriteAllBytesAsync(finalPath, doc);
#endif
    }

    [Test]
    public async Task Adding_inline_images_should_render_them_without_errors()
    {
        var generator = new PdfGeneratorBuilder()
            .WithDefaults()
            .Build();

        var imageBytes = await File.ReadAllBytesAsync("./Resources/SealOfApproval.jpg");
        var htmlTemplate = await File.ReadAllTextAsync("./Templates/TestTemplate.html");
        var base64Image = Convert.ToBase64String(imageBytes);

        var model = new SimplePage("Test image", base64Image, "Image");
        var generatorOptions = new PdfGeneratorOptionsBuilder<SimplePage>()
            .WithModel(model)
            .WithTemplate(htmlTemplate)
            .Build();

        var (html, doc) = await generator.GenerateDocumentAndIntermediateHtml(generatorOptions);

        doc
            .Should()
            .NotBeNullOrEmpty();
        html
            .Should()
            .Contain(model.Title);
        html
            .Should()
            .Contain(model.Base64Logo);
        html
            .Should()
            .Contain(model.Base64LogoAlt);

#if GenerateTestOutputFiles
        var finalPath = Path.Combine(_tmpPath, "inline_image_doc_test.pdf");
        await File.WriteAllBytesAsync(finalPath, doc);
#endif
    }

    [Test]
    public async Task Renderer_should_include_images_properly()
    {
        var imageBytes = await File.ReadAllBytesAsync("./Resources/SealOfApproval.jpg");
        var htmlTemplate = await File.ReadAllTextAsync("./Templates/TestTemplate.html");

        var base64Image = Convert.ToBase64String(imageBytes);
        var model = new SimplePage("Simple Page test", base64Image, "Nice.");
        var renderer = new FluidCoreTemplateRenderer();
        var renderResults = await renderer.RenderTemplate(htmlTemplate, model, Array.Empty<IAdditionalParserRule>());

        renderResults.FinalHtml
            .Should()
            .Contain(base64Image);
    }

#if SHARPDOC_REUSE_CHROME
    [Test]
    public async Task Should_throw_if_no_chrome_path_env_var_is_present()
    {
        var path = Environment.GetEnvironmentVariable("SHARPDOC_CHROME_PATH");
        Environment.SetEnvironmentVariable("SHARPDOC_CHROME_PATH", string.Empty);

        var generator = new PdfGeneratorBuilder()
            .WithDefaults()
            .Build();

        var options = new PdfGeneratorOptionsBuilder<DummyModel>()
            .WithModel(new DummyModel())
            .WithTemplate("")
            .Build();

        var act = async () => await generator.GenerateDocument(options);

        await act
            .Should()
            .ThrowAsync<PuppeteerSharpConverterException>();

        Environment.SetEnvironmentVariable("SHARPDOC_CHROME_PATH", path);
    }
#endif
}
