﻿using Squarecell.SharpDoc.Adapters.Defaults;
using Squarecell.SharpDoc.Contract.Ports;
using Squarecell.SharpDoc.Contract.Protocol;
using Squarecell.SharpDoc.TestLib;

namespace Squarecell.SharpDoc.Unit.Tests;

[TestFixture]
public class ParserRulesTest
{
    private readonly IGeneratePdf _generator;

    public ParserRulesTest()
    {
        _generator = new PdfGeneratorBuilder()
            .WithDefaults()
            .Build();
    }

    [Test]
    public async Task Adding_multiple_rules_at_once_should_not_cause_errors()
    {
        var htmlTemplate = await File.ReadAllTextAsync("./Templates/MultipleRules.html");
        const string fakeLogoStr = "FAKE LOGO";

        var simpleRule = new SimpleKeywordRule<DummyModel>("logo", _ => fakeLogoStr);
        var keywordRule = new KeywordWithIdentifierRule<DummyModel>("resource", (id, _) => $"FAKE {id}");

        var options = new PdfGeneratorOptionsBuilder<DummyModel>()
            .WithModel(new DummyModel())
            .WithTemplate(htmlTemplate)
            .AddParserRules(simpleRule, keywordRule)
            .Build();

        var (html, _) = await _generator.GenerateDocumentAndIntermediateHtml(options);

        html
            .Should()
            .Contain(fakeLogoStr);
        html
            .Should()
            .Contain("FAKE pouet");
    }

    [Test]
    public async Task Missing_rule_should_throw()
    {
        var htmlTemplate = await File.ReadAllTextAsync("./Templates/MissingRule.html");
        var options = new PdfGeneratorOptionsBuilder<DummyModel>()
            .WithModel(new DummyModel())
            .WithTemplate(htmlTemplate)
            .Build();

        Func<Task<FinalHtmlWithDoc>> act = async () => await _generator.GenerateDocumentAndIntermediateHtml(options);

        await act
            .Should()
            .ThrowAsync<FluidCoreTemplateRenderer.FluidCoreTemplateRendererException>();
    }

    [Test]
    public async Task Rule_with_identifier_should_correctly_map_values()
    {
        var map = new Dictionary<string, string> {{"plop", "Plop!"}, {"hi", "HIIIII!"}};
        var htmlTemplate = await File.ReadAllTextAsync("./Templates/RuleWithIdentifier.html");

        var options = new PdfGeneratorOptionsBuilder<DummyModel>()
            .WithModel(new DummyModel())
            .WithTemplate(htmlTemplate)
            .AddKeywordAndIdentifierRule("sellerResource", (s, _) => map[s])
            .Build();

        var (html, _) = await _generator.GenerateDocumentAndIntermediateHtml(options);

        html
            .Should()
            .Contain(map["plop"]);
        html
            .Should()
            .Contain(map["hi"]);
    }

    [Test]
    public async Task Simple_replace_rule_should_replace_text_in_template()
    {
        const string testStr = "HELLO!";

        var htmlTemplate = await File.ReadAllTextAsync("./Templates/SimpleReplaceRule.html");

        var options = new PdfGeneratorOptionsBuilder<DummyModel>()
            .WithModel(new DummyModel())
            .WithTemplate(htmlTemplate)
            .AddSimpleKeywordRule("putTestString", _ => testStr)
            .Build();

        var (html, _) = await _generator.GenerateDocumentAndIntermediateHtml(options);

        html
            .Should()
            .Contain(testStr);
    }
}
